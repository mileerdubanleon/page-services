<?php

namespace App\Http\Controllers;
use Validator, Hash, Auth;
use Illuminate\Http\Request;
use App\Models\User;

class ConnectController extends Controller
{
    // Constructor 
    public function __construct(){
        // Se establece un middleware llamado 'guest' que se aplicará a todas las rutas del controlador.
        // Los middleware en Laravel son capas intermedias que pueden procesar solicitudes 
        // HTTP antes de que lleguen a la ruta o controlador final.
		$this->middleware('guest')->except(['getLogout']);
        // El método except(['getLogout']) indica que todas las rutas del controlador, excepto 'getLogout', pasarán por el middleware 'guest'.
	}

    //Get Login
    public function getLogin() {
        return view('auth.login');
    }

	//Post Login
    public function postLogin(Request $request){
        //Validaciones, Mensajes de alerta
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];
        $messages = [
            'email.required' => 'Su correo electrónico es requerido.', 
            'email.email' => 'El formato de su correo electrónico es invalido',
            'password.required' => 'Por favor escriba una contraseña.',
            'password.min' => 'La contraseña debe tener al menos 8 caracteres.'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error')->with('typealert','danger'); 
        else:
            if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], true)):
                if(Auth::user()->status == "100"):
                    return redirect('/logout');
                else:
                    if(Auth::user()->role == "1"):
                        return redirect('/admin');
                    else:
                        return redirect('/');
                    endif;
                endif;
                else:
                    return back()->with('message','Correo electrónico o contraseña erronea')->with('typealert','danger');
            endif;
        endif;
    }

    //Logout cerrar sesion
    public function getLogout(){
        $status = Auth::user()->status;
        Auth::logout();
        if($status == "100"):
            return redirect('/login')->with('message','Su usuario fue suspendido.')->with('typealert', 'danger');
        else:
            return redirect('/');
        endif;
    }
}
