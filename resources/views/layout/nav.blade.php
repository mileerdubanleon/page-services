<nav class="navbar shadow navbar-expand-lg navbar-light bg-white position-fixed" style="height: 80px; width: 100%; z-index:99;">
    <div class="container-fluid px-5">
        <!-- Logo en la izquierda -->
        <a class="navbar-brand" href="#">Logo</a>

        <!-- Botón para móvil -->
        <button class="navbar-toggler" 
            type="button" 
            data-toggle="collapse" 
            data-target="#navbarNav" 
            aria-controls="navbarNav" 
            aria-expanded="false" 
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Menú centrado -->
        <div class="collapse navbar-collapse justify-content-center ml-5 pl-5 nav-movil" id="navbarNav">
            <!-- Botón de cierre -->
            <div class="d-lg-none">
                <button type="button" class="close close-btn mr-5 mt-5" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- Contenido del menú -->
            <ul class="navbar-nav border-right ul-movil" style="height: 30px;">
                <li class="nav-item align-items-center d-flex border-right">
                    <a id="inicio" class="nav-link mx-2 active" href="/home">Inicio</a>
                </li>
                <li class="nav-item align-items-center d-flex border-right">
                    <a id="nosotros" class="nav-link mx-2" href="/about">Nosotros</a>
                </li>
                <li class="nav-item align-items-center d-flex border-right">
                    <a id="servicios" class="nav-link mx-2" href="/services">Servicios</a>
                </li>
                <li class="nav-item align-items-center d-flex ">
                    <a id="masInfo" class="nav-link mx-2 btn btn-outline text-white" style="background-color: #002366;" href="/contact">Mas información</a>
                </li>
                <li class="nav-item mx-2 align-items-center d-flex ">
                    <a class="nav-link btn bg-danger text-white" style="border-radius: 0;" href="">+107 6572873</a>
                </li>
            </ul>

            <!-- Iconos de redes sociales a la derecha -->
            <div class="navbar-nav ml-3 d-none d-lg-flex">
                <a class="nav-link" href="#"><i class="fab fa-facebook"></i></a>
                <a class="nav-link" href="#"><i class="fab fa-whatsapp"></i></a>
            </div>
        </div>


    </div>
</nav>

<!-- Aquí se mostrará el contenido cargado dinámicamente -->
<div id="contenido">
    <div id="home">
        {{-- Cabecera web --}}
        @include('public.home')
    </div>

    <div id="about" style="display: none;">
        @include('public.about')
    </div>

    <div id="services" style="display: none;">
        <h1>Contenido de la página de Servicios</h1>
        <p>Este es el contenido de la página de Servicios.</p>
    </div>

    <div id="contact" style="display: none;">
        <h1>Contenido de la página de Contacto</h1>
        <p>Este es el contenido de la página de Contacto.</p>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
{{-- script nav dinamico --}}
<script>
    $(document).ready(function() {
        // Función para cargar el contenido de la página
        function cargarContenido(url) {
            // Ocultar todos los elementos de contenido
            $('#contenido > div').hide();
            
            // Mostrar el contenido correspondiente a la URL
            $('#' + url.replace('/', '')).show();
        }

        // Manejar clics en los enlaces del menú
        $('.navbar-nav .nav-link').click(function(e) {
            e.preventDefault(); // Prevenir el comportamiento predeterminado del enlace
            var url = $(this).attr('href'); // Obtener la URL del enlace
            $('.navbar-nav .nav-link').removeClass('active'); // Quitar la clase 'active' de todos los enlaces
            $(this).addClass('active'); // Agregar la clase 'active' al enlace actual
            cargarContenido(url); // Cargar el contenido de la página correspondiente
        });

        // Cargar el contenido de la página de inicio al cargar la página
        cargarContenido('/home');
        // boton cerrar collapse
        var closeButton = document.querySelector('.close-btn');
        var navbarCollapse = document.querySelector('.navbar-collapse');

        closeButton.addEventListener('click', function() {
            navbarCollapse.classList.remove('show'); // Oculta el menú
        });
    });
</script>
