<footer class="footer bg-dark text-light py-4">
    <div class="container">
        <div class="row">
            <!-- Sección 1: Logo -->
            <div class="col-md-3">
                <h1>logo</h1>
            </div>
            <!-- Sección 2: Información de contacto -->
            <div class="col-md-3">
                <h5>Información de Contacto</h5>
                <ul class="list-unstyled">
                    <li>Dirección: Calle Principal 123</li>
                    <li>Teléfono: +123456789</li>
                    <li>Email: info@empresa.com</li>
                </ul>
            </div>
            <!-- Sección 3: Información de la empresa -->
            <div class="col-md-3">
                <h5>Información de la Empresa</h5>
                <p>Somos una empresa dedicada a...</p>
            </div>
            <!-- Sección 4: Botón de contacto -->
            <div class="col-md-3 text-center">
                <button type="button" class="btn btn-outline-light">Regístrate aquí</button>
                <div class="d-flex justify-content-center">
                    <a class="nav-link" href="#"><i class="fab fa-facebook"></i></a>
                    <a class="nav-link" href="#"><i class="fab fa-whatsapp"></i></a>
                </div>
            </div>
            
        </div>
        <div>
            <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis 
                ipsum mollitia eum delectus. Velit, minima veritatis placeat, cum reiciendis 
                ea aut quidem vitae numquam in perspiciatis, enim voluptatibus adipisci 
                suscipit!
            </span>
        </div>
    </div>
</footer>
