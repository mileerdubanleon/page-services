
    <style>
        body, html {
            margin: 0;
            padding: 0;
            height: 100%;
            font-family: Arial, sans-serif;
        }

        .parallax-container {
            position: relative;
            height: 70vh; /* Altura del parallax */
            overflow: hidden;
        }

        .parallax-background {
            width: 100%;
            height: auto;
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;
            transition: transform 0.4s ease; /* Transición suave */
        }

        .overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(31, 29, 29, 0.397); /* Fondo blanco con opacidad */
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .container {
            position: relative;
            z-index: 1;
        }
    </style>
    {{-- one image parallax --}}
    <div class="parallax-container">
        <img src="https://united-studies.com/wp-content/uploads/2018/04/dreamstime_m_68297745.jpg" 
            alt="Imagen de ejemplo" 
            class="parallax-background img-responsive" 
            data-speed="0.2"
        >
        <div class="overlay">
            <div class="container my-5">
                <h2 class="text-white">Contenido encima del parallax</h2>
                <p class="text-white">Este es el contenido que se mostrará encima del efecto parallax.</p>
            </div>
        </div>
    </div>

    {{-- Sección con imagen y texto --}}
    <div class="container-fluid my-5 px-5">
        <div class="row">
            <div class="col-md-6">
                <img src="/static/img/img1.jpg" style="width: 740px; height: 350px;" class="shadow img-fluid" alt="Img">
            </div>
            <div class="col-md-6">
                <p class="text-center my-4">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatibus officia expedita a similique totam unde reprehenderit earum cum, quibusdam saepe molestias necessitatibus sed sunt dolorem sapiente impedit illum quo temporibus.
                </p>
                <p class="text-center my-1">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatibus officia expedita a similique totam unde reprehenderit earum cum, quibusdam saepe molestias necessitatibus sed sunt dolorem sapiente impedit illum quo temporibus.
                </p>
                <div class="text-center">
                    <button type="button" class="btn btn-outline-danger">
                        Llámanos +107 6572873
                    </button>
                </div>
            </div>
        </div>
    </div>

    {{-- img parallax --}}
    <div class="parallax-container" style="height: 200px;">
        <img src="https://www.timeshighereducation.com/sites/default/files/styles/the_breaking_news_image_style/public/american-students-posing-with-usa-stars-and-stripes-flag.jpg?itok=_CwZVfO6" 
            alt="Imagen de ejemplo" 
            class="parallax-background img-responsive" 
            data-speed="0.4"
        >
        <div class="overlay">
            <div class="container my-5">
                <h2 class="text-white">Contenido encima del parallax</h2>
                <p class="text-white">Este es el contenido que se mostrará encima del efecto parallax.</p>
            </div>
        </div>
    </div>

    {{-- Sección con imagen y texto --}}
    <div class="container my-5">
        <div class="row">
            <div class="col-md-6">
                <img src="https://via.placeholder.com/640x360" style="width: 640px; height: 360px;" class="img-fluid" alt="Imagen de ejemplo">
            </div>
            <div class="col-md-6">
                <p class="text-center my-4">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatibus officia expedita a similique totam unde reprehenderit earum cum, quibusdam saepe molestias necessitatibus sed sunt dolorem sapiente impedit illum quo temporibus.
                </p>
                <p class="text-center my-1">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatibus officia expedita a similique totam unde reprehenderit earum cum, quibusdam saepe molestias necessitatibus sed sunt dolorem sapiente impedit illum quo temporibus.
                </p>
                <div class="text-center">
                    <button type="button" class="btn btn-outline-danger">
                        Llámanos +107 6572873
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var parallaxElements = document.querySelectorAll('.parallax-background');
            window.addEventListener('scroll', function() {
                parallaxElements.forEach(function(element) {
                    var speed = parseFloat(element.getAttribute('data-speed'));
                    var yPos = -window.scrollY * speed;
                    element.style.transform = 'translateY(' + yPos + 'px)';
                });
            });
        });
    </script>

