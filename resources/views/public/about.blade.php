<div class="parallax-container">
    <img src="https://via.placeholder.com/1920x1080" alt="Imagen de ejemplo" class="parallax-background">
    <div class="overlay">
        <div class="container my-5">
            <h2>Contenido encima de Nosotros</h2>
            <p>Este es el contenido que se mostrará encima del efecto parallax.</p>
        </div>
    </div>
</div>