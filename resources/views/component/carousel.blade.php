<div id="carouselExampleControls" class="carousel slide mt-5" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <span class="text1"><b>Haz tu pedido con tiempo</b><br>
                Asegúrate de que tus pedidos lleguen a tiempo para las fiestas. Comprar Ayuda.
            </span>
        </div>
        <div class="carousel-item">
            <span>Los estudiantes tienen un 10 % de descuento
                <br><a href=""> Más información.</a>
            </span>
        </div>
        <div class="carousel-item">
            <span>Envíos y devoluciones gratis para Members en un plazo de 60 días. <br><a href=""> Únete para más información</a>
            </span>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>