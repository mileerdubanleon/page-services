@extends('auth.master')

@section('title', 'login')
@section('content')

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!--Alert--->
@if(Session::has('message') || $errors->has('email'))
    <div class="position-fixed d-flex justify-content-center text-align-center w-100">
        <div class="w-100 alert alert-{{ Session::get('typealert') }}" style="display:none;">
            @if(Session::has('message'))
                <span>
                    ¡Error!
                </span>
            @endif

            <div class="">
                @if($errors->has('email'))
                    {{ $errors->first('email') }}
                @endif
            </div>

            @if($errors->has('password'))
                {{ $errors->first('password') }}
            @endif

            <script>
                $('.alert').slideDown();
                setTimeout(function(){ $('.alert').slideUp(); }, 10000)
            </script>
        </div>
    </div>
@endif

{{-- login --}}
<div class="w-full d-flex justify-content-center align-items-center" style="height: 100vh;">
    
    <div class="container" style="max-width: 500px; height: 100vh;">
        <h3 class="container-fluid mt-5 pt-5" style="font-weight: 400;">Introduce tu dirección de correo electrónico para iniciar sesión.</h3>

        <form method="POST" action="{{ url('/login') }}">
            @csrf
    
                <input class="form my-5 d-flex container input-connect"  type="email" name="email" placeholder="Correo electrónico*">

                <input class="form mt-5 d-flex container input-connect"  type="password" name="password" placeholder="Contraseña">

                <div class="d-flex text-start mt-2 mb-5 mx-3" style="cursor: pointer">
                    <a class="toggle-password view-password" style="color: #333; float: right; margin-left: 270px;">Mostrar Contraseña</a>
                </div>

                <div class="container text-center" style="font-size: 12px;">
                    Al continuar, acepto la Política de privacidad y los Términos de uso de la Tienda.
                </div>

                <div class="my-3 mx-4 d-flex position-links">
                    <a href="{{ url('/register') }}" class="col text-center font-weight-bold" style="color: #000;font-size: 13px;">
                        Crea una cuenta
                    </a>
                    <a href="#" class="col font-weight-bold text-center" style="color: #000; font-size: 13px;">
                        Olvide mi contraseña
                    </a>
                </div>

                <div class="container ">
                    <button type="submit" class="btn btn-outline-dark form-control">Iniciar sesión</button>
                </div>
        </form>

    </div>

    

</div>



    
    