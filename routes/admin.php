<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProductGalleryController;
use App\Http\Controllers\Admin\SettingsController;

Route::prefix('/admin')->group(function () {
    // View dashboard admin
    Route::get('/', [DashboardController::class, 'getDashboard'])->name('dashboard');

    // Module Users
    // GET
    Route::get('/users/{status}', [UserController::class, 'getUsers'])->name('user_list');

	
});
?>
