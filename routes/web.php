<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConnectController;

Route::get('/', function () {
    return view('index');
})->name('home');

// Route Auth
Route::get('/login', [ConnectController::class, 'getLogin'])->name('login');
Route::post('/login', [ConnectController::class, 'postLogin']);
Route::get('/logout', [ConnectController::class, 'getLogout'])->name('logout');


